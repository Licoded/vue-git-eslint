# vue-git-eslint

## 功能介绍

1. `git commit` 前 `ESLint` 检测
2. `git commit` 注释信息强制规范

![commit](./assets/commit.png)

## 使用步骤

```bash
git clone https://gitlab.com/Licoded/vue-git-eslint.git --depth=1 <your-project-name>
cd <your-project-name>
# 执行下面命令使 husky 的 git 钩子生效
# 无需手动执行此命令，在你执行任何一条 npm 命令后会自动执行 prepare 脚本命令
npm run prepare
```
